class WelcomeController < ApplicationController
  before_action :array_init, only: :result

  def index
  end

  def result
  end

  private

    def array_init
      @array = params[:array].split(/[\s,.]+/) if params[:array]
    end

end
