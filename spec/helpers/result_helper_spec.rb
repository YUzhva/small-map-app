require 'rails_helper'

RSpec.describe ResultHelper, type: :helper do

  describe "#find_ident" do
    it "returns the instance variable" do
      splited_array = ['Saab', 'Volvo', 'BMW', 'Saab', '1', '2', '2']
      expect(helper.find_ident(splited_array)).to include('Saab' => 2, '2' => 2, 'Volvo' => 1)
    end
  end

end
